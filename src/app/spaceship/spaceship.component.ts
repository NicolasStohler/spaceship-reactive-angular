import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { BehaviorSubject, combineLatest, fromEvent, interval, merge, Observable, range } from 'rxjs';
import {
  distinctUntilChanged,
  filter,
  flatMap,
  map,
  sampleTime,
  scan,
  startWith,
  takeWhile,
  tap,
  timestamp,
  toArray
} from 'rxjs/operators';

const SPEED = 40;
const STAR_NUMBER = 250;
const SCORE_INCREASE = 10;

const SHOOTING_SPEED = 15;

const ENEMY_FREQ = 1500;
const ENEMY_SHOOTING_FREQ = 750;

@Component({
  selector: 'app-spaceship',
  template: '<canvas #canvasX class="game"></canvas>',
  // templateUrl: './spaceship.component.html',
  styleUrls: ['./spaceship.component.css']
})
export class SpaceshipComponent implements OnInit, AfterViewInit  {

  @ViewChild('canvasX') public canvasEl: ElementRef;  // connect to canvas in template

  private canvas: HTMLCanvasElement;
  private ctx: CanvasRenderingContext2D;

  private HERO_Y = 0;

  constructor() { }

  ngOnInit() {
    console.log('init');
  }

  ngAfterViewInit(): void {
    console.log('after view init');
    this.canvas = HTMLCanvasElement = this.canvasEl.nativeElement;
    this.ctx = this.canvas.getContext('2d');

    // make fullscreen
    this.canvas.width = window.innerWidth;
    this.canvas.height = window.innerHeight;

    this.HERO_Y = this.canvas.height - 30;

    this.startGame();
  }

  private startGame() {{}
    console.log('start game');

    const starStream$ = this.getStarStream$();
    const mouseMove$ = fromEvent(this.canvas, 'mousemove') as Observable<MouseEvent>;
    const spaceShip$ = this.getSpaceShip(mouseMove$);
    const enemies$ = this.getEnemies();
    const playerFiring$ = this.getPlayerFiring();
    const heroShots$ = this.getHeroShots(playerFiring$, spaceShip$);

    const scoreSubject = new BehaviorSubject<number>(0);
    const score$ = scoreSubject.pipe(
      scan((prev, curr) => prev + curr, 0)
    );

    const game$ = combineLatest(
      starStream$,
      spaceShip$,
      enemies$,
      heroShots$,
      score$
    ).pipe(
      map(([stars, spaceship, enemies, heroShots, score]) => ({
        stars,
        spaceship,
        enemies,
        heroShots,
        score
      })),
      sampleTime(SPEED),
      takeWhile(actors => this.gameOver(actors.spaceship, actors.enemies) === false)
    );

    game$.subscribe((actors) => this.renderScene(actors, scoreSubject));
  }

  private renderScene(actors, scoreSubject: BehaviorSubject<number>) {
    this.paintStars(actors.stars);
    this.paintSpaceShip(actors.spaceship.x, actors.spaceship.y);
    this.paintEnemies(actors.enemies);
    this.paintHeroShots(actors.heroShots, actors.enemies, scoreSubject);
    this.paintScore(actors.score);
  }

  private getStarStream$(): Observable<any> {
    return range(1, STAR_NUMBER).pipe(
      map(() => ({
        x: Math.round(Math.random() * this.canvas.width),
        y: Math.round(Math.random() * this.canvas.height),
        size: Math.random() * 3 + 1
      })),
      toArray(),
      flatMap(starArray =>
        interval(SPEED).pipe(
          map(() => {
            starArray.forEach(star => {
              if (star.y >= this.canvas.height) {
                star.y = 0; // Reset star to top of the screen
              }
              star.y += star.size; // Move star
            });
            return starArray;
          })
        )
      ),
    );
  }

  private getSpaceShip(mouseMove$: Observable<MouseEvent>): Observable<any> {
    return mouseMove$.pipe(
      map(event => ({
        x: event.clientX,
        y: this.HERO_Y
      })),
      startWith({
        x: this.canvas.width / 2,
        y: this.HERO_Y
      })
    );
  }

  private getEnemies(): Observable<any[]> {
    const enemies$ = interval(ENEMY_FREQ).pipe(
      scan((enemyArray: any[], curr: any) => {
        const enemy = {
          x: Math.round(Math.random() * this.canvas.width),
          y: -30,
          shots: [],
          isDead: false
        };

        interval(ENEMY_SHOOTING_FREQ).subscribe(() => {
          if (!enemy.isDead) {  // can only shoot if not dead
            enemy.shots.push({ x: enemy.x, y: enemy.y });
          }
          enemy.shots = enemy.shots.filter(s => this.isVisible(s));
        });

        enemyArray.push(enemy);
        return enemyArray
          .filter(e => this.isVisible(e))   // no need to draw if outside of screen boundaries
          .filter(_enemy => !(_enemy.isDead && _enemy.shots.length === 0)) // keep updating shots for dead enemies
          ;
      }, [])
    );

    return enemies$;
  }

  private getPlayerFiring(): Observable<any> {
    return merge(
      fromEvent(this.canvas, 'click'),
      fromEvent(document, 'keydown').pipe(
        filter((event: KeyboardEvent) => event.keyCode === 32) // spacebar
      )
    ).pipe(
      tap(() => console.log('fire!')),
      startWith({}),
      sampleTime(200),
      timestamp()
    );
  }

  private getHeroShots(playerFiring$: Observable<any>, spaceShip$: Observable<any>) {
    return combineLatest(
      playerFiring$,
      spaceShip$
    ).pipe(
      map(([shotEvents, spaceShip]) => ({
        timestamp: shotEvents.timestamp,
        x: spaceShip.x
      })),
      // distinctUntilChanged(shot => shot.timestamp),  // rxjs 6: not working anymore
      distinctUntilChanged((x, y) => x.timestamp === y.timestamp),
      scan((shotArray: any[], shot: any) => {
        shotArray.push({
          x: shot.x,
          y: this.HERO_Y
        });
        return shotArray;
      }, [])
    );
  }

  private drawTriangle(x, y, width, color, direction) {
    this.ctx.fillStyle = color;
    this.ctx.beginPath();
    this.ctx.moveTo(x - width, y);
    this.ctx.lineTo(x, direction === 'up' ? y - width : y + width);
    this.ctx.lineTo(x + width, y);
    this.ctx.lineTo(x - width, y);
    this.ctx.fill();
  }

  private paintSpaceShip(x, y) {
    this.drawTriangle(x, y, 20, '#ff0000', 'up');
  }

  private paintStars(stars) {
    this.ctx.fillStyle = '#000000';
    this.ctx.fillRect(0, 0, this.canvas.width, this.canvas.height);
    this.ctx.fillStyle = '#ffffff';
    stars.forEach(star => {
      this.ctx.fillRect(star.x, star.y, star.size, star.size);
    });
  }

  private paintEnemies(enemies: any) {
    enemies.forEach(enemy => {
      // paint enemy ship
      enemy.y += 5;
      enemy.x += this.getRandomInt(-15, 15);

      if (!enemy.isDead) {
        this.drawTriangle(enemy.x, enemy.y, 20, '#00ff00', 'down');
      }

      // paint enemy shots
      enemy.shots.forEach(shot => {
        shot.y += SHOOTING_SPEED;
        this.drawTriangle(shot.x, shot.y, 5, 'magenta', 'down');
      });
    });
  }

  private paintHeroShots(heroShots, enemies: any[], scoreSubject: BehaviorSubject<number>) {
    heroShots.forEach(shot => {
      enemies.forEach(enemy => {
        if (!enemy.isDead && this.collision(shot, enemy)) {
          scoreSubject.next(SCORE_INCREASE);
          enemy.isDead = true;
          shot.x = shot.y = -100;   // move shot outside of screen
          // break;  // why? (for for loop...)
        }
      });

      shot.y -= SHOOTING_SPEED;
      this.drawTriangle(shot.x, shot.y, 5, '#ffff00', 'up');
    });
  }

  private gameOver(ship, enemies) {
    return enemies.some(enemy => {
      if (this.collision(ship, enemy)) {
        return true;
      }

      return enemy.shots.some(shot => this.collision(ship, shot));
    });
  }

  // Helper function to get a random integer
  private getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  private isVisible(obj) {
    return obj.x > -40 &&
      obj.x < this.canvas.width + 40 &&
      obj.y > -40 &&
      obj.y < this.canvas.height + 40;
  }

  private collision(target1, target2) {
    return (
      target1.x > target2.x - 20 &&
      target1.x < target2.x + 20 &&
      (target1.y > target2.y - 20 && target1.y < target2.y + 20)
    );
  }

  private paintScore(score) {
    this.ctx.fillStyle = '#ffffff';
    this.ctx.font = 'bold 26px sans-serif';
    this.ctx.fillText(`Score: ${score}`, 40, 43);
  }
}
